FROM node:8

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

RUN cp ./src/parameters.json.dist ./src/parameters.json

RUN npm run build

EXPOSE 3000

CMD [ "npm", "start" ]