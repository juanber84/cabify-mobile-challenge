'use strict'

import axios from 'axios'
let authentication = null

let baseUrl = ''
if (process && process.env && process.env.NODE_ENV === 'development') {
    baseUrl = 'http://localhost:3000'
}

export default {
    async getProducts(){
        let response = await axios.get(baseUrl + '/products')
        let products = response.data
        return products
    },
    async scanProduct(code) {
        let opts = {}
        if (authentication)
            opts.headers= authentication
        let response = await axios.post(baseUrl + '/products', code, opts)
        if (response && response.headers && response.headers.authentication)
            authentication = { authentication: response.headers.authentication }
        let checkout = response.data
        if (checkout.list) {
            checkout.discount = checkout.list.reduce(function (acc, obj) {
                if (!obj.discount) obj.discount = 0
                return acc + obj.discount
            }, 0)
        }
        return checkout
    },
    async getTotal() {
        let opts = {}
        if (authentication)
            opts.headers = authentication        
        let response = await axios.get(baseUrl + '/products/total', opts)
        let total = response.data
        return total
    },     
}