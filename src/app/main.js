import Vue from 'vue'
import Router from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './main.css'
import Docs from './components/Docs'
import Shop from './components/Shop'

Vue.use(Router)
Vue.use(BootstrapVue)
Vue.config.productionTip = false

let router = new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Docs
    },
    {
      path: '/doc/:page',
      name: 'Docs',
      component: Docs
    },    
    {
      path: '/shop',
      name: 'Shop',
      component: Shop
    }    
  ]
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
