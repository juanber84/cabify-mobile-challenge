'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const path = require('path')

const app = express()
app.use(bodyParser.json())

const corsOptions = {
    exposedHeaders: 'Authentication',
}
app.use(cors(corsOptions))

const mainController = require('./controllers/main')
const checkoutController = require('./controllers/checkout')

app.use(express.static(path.join(__dirname + './../../dist')))
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + './../../dist/index.html'));
})
app.get('/health', mainController.index)
app.get('/products', checkoutController.listProducts)
app.post('/products', checkoutController.addProduct)
app.get('/products/total', checkoutController.total)

module.exports = app
