'use strict'

const _ = require('lodash')
let storeData = {}
const randomToken = require('random-token')
const dataProducs = require('../../../data/products')
const dataRules = require('../../../data/rules')
const Checkout = require('./../../lib/Checkout')

module.exports = {
  listProducts(req, res) {
    res.json(dataProducs)
  },

  addProduct(req, res) {
    // Resolve authentication
    let authentication = _.get(req, 'headers.authentication')
    if (!authentication) {
      authentication = randomToken(16)
    }
    // Get store
    let checkout = storeData[authentication]
    if (!checkout) {
      checkout = new Checkout(dataRules)
      storeData[authentication] = checkout
    }
    // Get product in body
    let codeProduct = req.body.code
    checkout.scan(codeProduct)
    // Return response
    res.set('authentication', authentication)
    res.json({
      list: checkout.list(),
      total: checkout.total(),
    })
  },

  total(req, res) {
    // Resolve authentication
    let authentication = _.get(req, 'headers.authentication')
    if (!authentication) {
      authentication = randomToken(16)
    }
    // Get store
    let checkout = storeData[authentication]
    if (!checkout) {
      checkout = new Checkout(dataRules)
      storeData[authentication] = checkout
    }
    // Return response
    res.set('authentication', authentication)
    res.json({
      list: checkout.list(),
      total: checkout.total(),
    })
  },
}
