'use strict'

const { listenPort } = require('./parameters')
const app = require('./server')

app.listen(listenPort, function() {
  console.log('Example app listening on port ' + listenPort + '!')
})
