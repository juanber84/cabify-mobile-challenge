'use strict'

module.exports = [
  {
    name: 'halfPrice',
    condition: (obj, data) => {
      return obj.code === data.code && obj.units >= 2
    },
    consequence: obj => {
      let pairs = Math.trunc(obj.units / 2)
      obj.discount = pairs * obj.price
    },
  },
  {
    name: 'changePrice',
    condition: (obj, data) => {
      return obj.code === data.code && obj.units >= data.units
    },
    consequence: (obj, data) => {
      obj.discount = (obj.price - data.newPrice) * obj.units
    },
  },
]
