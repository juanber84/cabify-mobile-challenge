'use strict'

const dataProducts = require('../../data/products.json')
const rulesPrices = require('./rulesPrices')

class Checkout {
  constructor(pricingRules) {
    this.products = []
    this.pricingRules = pricingRules
  }

  scan(productCode) {
    let product = dataProducts.find(p => {
      return p.code === productCode
    })
    if (!product) throw new Error('Product not exist')

    if (
      !this.products.find(p => {
        if (p.code === product.code) {
          p.units++
          return true
        }
        return false
      })
    ) {
      this.products.push({
        code: product.code,
        price: product.price,
        units: 1,
      })
    }
  }

  _applyPricingRules() {
    this.pricingRules.forEach(r => {
      let rule = rulesPrices.find(rp => {
        return rp.name === r.name
      })
      this.products.forEach(p => {
        if (rule.condition(p, r.opts)) {
          rule.consequence(p, r.opts)
        }
      })
    })
  }

  list() {
    return this.products
  }

  total() {
    if (this.pricingRules) this._applyPricingRules()
    return this.products.reduce(function(acc, obj) {
      if (!obj.discount) obj.discount = 0
      return acc + obj.price * obj.units - obj.discount
    }, 0)
  }
}

module.exports = Checkout
