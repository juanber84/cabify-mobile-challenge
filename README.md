### Cabify Mobile Challenge
====================================================================================================================

##### Documentation
- [Requirements](doc/requirements.md)
- [Teamwork solution](doc/teamwork-solution.md)
- [Programming Language](doc/programming-language.md)
- [Database](doc/database.md)
- [Continuous Integration](doc/continuous-integration.md)
- [Virtualization](doc/virtualization.md)
- [Quality tools](doc/quality-tools.md)

```

```

##### Installation and usage from git repository. IMPORTANT, you must have installed in your computed node >= 8.9 and npm/yarn 

- Download the code 
```
git clone https://bitbucket.org/juanber84/cabify-mobile-challenge.git
```

- Move to the folder
```
cd cabify-mobile-challenge
```

- Copy parameters and modify them or not
```
cp ./src/parameters.json.dist ./src/parameters.json
cp ./test/extra/parameters.json.dist ./test/extra/parameters.json 
```

- Install dependencies
```
npm install / yarn
```

- To compile the frontend app run the next command
```
npm run build / yarn build
```

- Start the project
```
npm start / yarn start
```

- Open the url in your preferer browser
[http://localhost:3000](http://localhost:3000)

```

```

##### Installation and usage from docker image
- Pull the image in your computer
```
docker pull juanber84/cabify-mobile-challenge:master
```
- Run the image. Important, by default the image expose 3000 port.
```
docker run -p 3000:3000 juanber84/cabify-mobile-challenge:master
```
- Open the url in your preferer browser
[http://localhost:3000](http://localhost:3000)

```

```

====================================================================================================================
```                 
{
    "name": "Juan Berzal Sacristan",
    "email": "juanber84@gmail.com",
    "github": "github.com/juanber84"
    "website": "www.juanberzal.com"
}
```