### Cabify Mobile Challenge
====================================================================================================================

#### Programming Language

The challenge of code is writen in **nodejs** languaje. The **project** is structured like this.

```
+ .circleci (ci)
+ data (database simulation)
+ dist (frontend compilation code)
+ doc (markdown documentation)
+ src (main code)
    - app (frontend app code)
    - lib (main checkout code libraries)
    - server (api rest code)
+ reports (testing reports)
+ test (testing code)
```

#### Main Files

```javascript

// src/lib/Checkout.js

'use strict'

const dataProducts = require('../../data/products.json')
const rulesPrices = require('./rulesPrices')

class Checkout {
  constructor(pricingRules) {
    this.products = []
    this.pricingRules = pricingRules
  }

  scan(productCode) {
    let product = dataProducts.find(p => {
      return p.code === productCode
    })
    if (!product) throw new Error('Product not exist')

    if (
      !this.products.find(p => {
        if (p.code === product.code) {
          p.units++
          return true
        }
        return false
      })
    ) {
      this.products.push({
        code: product.code,
        price: product.price,
        units: 1,
      })
    }
  }

  _applyPricingRules() {
    this.pricingRules.forEach(r => {
      let rule = rulesPrices.find(rp => {
        return rp.name === r.name
      })
      this.products.forEach(p => {
        if (rule.condition(p, r.opts)) {
          rule.consequence(p, r.opts)
        }
      })
    })
  }

  list() {
    return this.products
  }

  total() {
    if (this.pricingRules) this._applyPricingRules()
    return this.products.reduce(function(acc, obj) {
      if (!obj.discount) obj.discount = 0
      return acc + obj.price * obj.units - obj.discount
    }, 0)
  }
}

module.exports = Checkout

```

This is the main class in which **store** the products and **calculate** the total price following the rules.

The code is *based in reference* documentation. It is simple, you can insert the **rules** and **scan** one to one the products.

Implemented extra **list** method to return the products (required in rest/front apps). 

Next improvements:
-  Method to remove products
-  Stocks in products

```javascript

// src/lib/rulesPrices.js

'use strict'

module.exports = [
  {
    name: 'halfPrice',
    condition: (obj, data) => {
      return obj.code === data.code && obj.units >= 2
    },
    consequence: obj => {
      let pairs = Math.trunc(obj.units / 2)
      obj.discount = pairs * obj.price
    },
  },
  {
    name: 'changePrice',
    condition: (obj, data) => {
      return obj.code === data.code && obj.units >= data.units
    },
    consequence: (obj, data) => {
      obj.discount = (obj.price - data.newPrice) * obj.units
    },
  },
]

```

Module which *exports* an array with differents **rules**.

Every rule must have a excluded id **name** and two methods, **condition** and **consequence**.

If you want to **extend** the rules you must add a new element in this array.

Next improvements:
- Possibility of apply different rules to the same product
- Compatibility or incompatibility between rules

#### Extra ball

Api rest built with **express** like **sinatra** in ruby or **gin** on go.
```
// src/server
```
Single page app built with **vue**
```
// src/app
```


```

```

====================================================================================================================
```                 
{
    "name": "Juan Berzal Sacristan",
    "email": "juanber84@gmail.com",
    "github": "github.com/juanber84"
    "website": "www.juanberzal.com"
}
```