### Cabify Mobile Challenge
====================================================================================================================

#### Teamwork Solution

I tried to write my code as if it was a production code, for that reason I started it with a little **scrum canvan** in **Trello** plattform. 
The first development is in the issues tagged with [MVP] tag and after that, I extended this with differents *features*.

[https://trello.com/b/XBpeiWIF/cabify-mobile-challenge](https://trello.com/b/XBpeiWIF/cabify-mobile-challenge)

The code of course must be in a version control system. I chose **git** in **Bitbucket** plattform. You can see the code in this url:

[https://bitbucket.org/juanber84/cabify-mobile-challenge/src/master/](https://bitbucket.org/juanber84/cabify-mobile-challenge/src/master/)

```

```

====================================================================================================================
```                 
{
    "name": "Juan Berzal Sacristan",
    "email": "juanber84@gmail.com",
    "github": "github.com/juanber84"
    "website": "www.juanberzal.com"
}
```