### Cabify Mobile Challenge
====================================================================================================================

#### Quality Tools

In the project I have included some tools to get the highest quality, for example.

##### Code Style Guide
- **prettier**
- **lint**
- **validate-commit**

##### Testing, unit and functional
The tests is writing with **jest**. You can execute it with in the terminal.
```
npm run test / yarn test
```

##### Code coverage
The **lcov** file is generated with jest and uploaded to **codecov** plattform.
You can see the results in this link.

[https://codecov.io/bb/juanber84/cabify-mobile-challenge](https://codecov.io/bb/juanber84/cabify-mobile-challenge)

```

```

====================================================================================================================
```                 
{
    "name": "Juan Berzal Sacristan",
    "email": "juanber84@gmail.com",
    "github": "github.com/juanber84"
    "website": "www.juanberzal.com"
}
```