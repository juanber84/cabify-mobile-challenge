### Cabify Mobile Challenge
====================================================================================================================

#### Virtualization

Docker is best simple way to mount the code and distribute it.

This is the docker file content:

```
// Dockerfile

FROM node:8

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD [ "npm", "start" ]
```

This image is public in docker hub and you can see it here

[https://hub.docker.com/r/juanber84/cabify-mobile-challenge/](https://hub.docker.com/r/juanber84/cabify-mobile-challenge/)

```

```

====================================================================================================================
```                 
{
    "name": "Juan Berzal Sacristan",
    "email": "juanber84@gmail.com",
    "github": "github.com/juanber84"
    "website": "www.juanberzal.com"
}
```