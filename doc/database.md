### Cabify Mobile Challenge
====================================================================================================================

#### Database

In this case the database is a simple file system database, with two simple json files.

```
// data/products.json

[
    {
        "code": "VOUCHER", 
        "name": "Cabify Voucher",
        "price": 5.00
    },
    {
        "code": "TSHIRT",
        "name": "Cabify T-Shirt",
        "price": 20.0
    },
    {
        "code": "MUG",
        "name": "Cabify Coffee Mug",
        "price": 7.50
    }    
]


// data/rules.json

[
    {
        "name": "halfPrice",
        "opts": {
            "code": "VOUCHER"
        }
    },
    {
        "name": "changePrice",
        "opts": {
            "code": "TSHIRT",
            "units": 3,
            "newPrice": 19.0
        }
    }
]
```

```

```

====================================================================================================================
```                 
{
    "name": "Juan Berzal Sacristan",
    "email": "juanber84@gmail.com",
    "github": "github.com/juanber84"
    "website": "www.juanberzal.com"
}
```