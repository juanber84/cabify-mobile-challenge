### Cabify Mobile Challenge
====================================================================================================================

#### Continuous Integration

I have used circleci to generate docker image of the project and upload it to docker hub.

The step runner contains:
- Installation of project
- Execution of test
- Generation of code coverage reports
- Build and upload images

You can see all the deploys if you click in this link

[https://circleci.com/bb/juanber84/cabify-mobile-challenge](https://circleci.com/bb/juanber84/cabify-mobile-challenge)

```

```

====================================================================================================================
```                 
{
    "name": "Juan Berzal Sacristan",
    "email": "juanber84@gmail.com",
    "github": "github.com/juanber84"
    "website": "www.juanberzal.com"
}
```