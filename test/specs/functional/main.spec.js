'use strict'

const app = require('../../../src/server/index')
const supertest = require('supertest')

describe('Rest', function() {

  beforeEach(async () => {
    this.request = supertest.agent(app)
  })

  it('GET /health ', async () => {
    let response = await this.request.get('/health')
    expect(response.body).toBe('hi')
  })
  
})
