'use strict'

const app = require('../../../src/server/index')
const supertest = require('supertest')

describe('Rest', function() {
  beforeEach(async () => {
    this.request = supertest.agent(app)
  })

  it('GET /products ', async () => {
    let response = await this.request.get('/products')
    expect(response.status).toBe(200)
    expect(response.body).toMatchSchema({
      type: 'array',
      items: {
        type: 'object',
        properties: {
          code: {
            type: 'string',
          },
          name: {
            type: 'string',
          },
          price: {
            type: 'number',
          },
        },
      },
    })
  })

  it('POST /products ', async () => {
    let response = await this.request.post('/products').send({
      code: 'VOUCHER',
    })
    expect(response.status).toBe(200)
    expect(response.body.list[0].code).toBe('VOUCHER')
    expect(response.body.list[0].units).toBe(1)
    if (response.header && response.header.authentication)
      this.authentication = response.header.authentication
  })

  it('POST /products again', async () => {
    let response = await this.request
      .post('/products')
      .set({ authentication: this.authentication, Accept: 'application/json' })
      .send({
        code: 'VOUCHER',
      })
    expect(response.status).toBe(200)
    expect(response.body.list[0].code).toBe('VOUCHER')
    expect(response.body.list[0].units).toBe(2)
    if (response.header && response.header.authentication)
      this.authentication = response.header.authentication
  })

  it('POST /products/total', async () => {
    let response = await this.request
      .get('/products/total')
      .set({ authentication: this.authentication, Accept: 'application/json' })
    expect(response.status).toBe(200)
    expect(response.body.list[0].code).toBe('VOUCHER')
    expect(response.body.list[0].units).toBe(2)
    expect(response.body.total).toBe(5)
  })
})
