'use strict'

const Checkout = require('../../../src/lib/Checkout')
const rules = require('../../../data/rules')

describe('checkout', function() {

  it('Empty checkout', async () => {
    let newCheckout = new Checkout(rules)
    let total = newCheckout.total()
    expect(total).toBe(0)
  })

  it('Invalid product', async () => {
    let newCheckout = new Checkout(rules)
    expect(() => {
      newCheckout.scan('INVALID')
    }).toThrow()
  })

  it('Items: VOUCHER, TSHIRT, MUG Total: 32.50€', async () => {
    let newCheckout = new Checkout(rules)
    newCheckout.scan('VOUCHER')
    newCheckout.scan('TSHIRT')
    newCheckout.scan('MUG')
    let total = newCheckout.total()
    expect(total).toBe(32.5)
  })

  it('Items: VOUCHER, TSHIRT, VOUCHER Total: 25.00€', async () => {
    let newCheckout = new Checkout(rules)
    newCheckout.scan('VOUCHER')
    newCheckout.scan('TSHIRT')
    newCheckout.scan('VOUCHER')
    let total = newCheckout.total()
    expect(total).toBe(25.0)
  })


  it('Items: TSHIRT, TSHIRT, TSHIRT, VOUCHER, TSHIRT Total: 85.00€ Without rules', async () => {
    let newCheckout = new Checkout()
    newCheckout.scan('TSHIRT')
    newCheckout.scan('TSHIRT')
    newCheckout.scan('TSHIRT')
    newCheckout.scan('VOUCHER')
    newCheckout.scan('TSHIRT')
    let total = newCheckout.total()
    expect(total).toBe(85.0)
  })

  it('Items: TSHIRT, TSHIRT, TSHIRT, VOUCHER, TSHIRT Total: 81.00€', async () => {
    let newCheckout = new Checkout(rules)
    newCheckout.scan('TSHIRT')
    newCheckout.scan('TSHIRT')
    newCheckout.scan('TSHIRT')
    newCheckout.scan('VOUCHER')
    newCheckout.scan('TSHIRT')
    let total = newCheckout.total()
    expect(total).toBe(81.0)
  })

  it('Items: VOUCHER, TSHIRT, VOUCHER, VOUCHER, MUG, TSHIRT, TSHIRT Total: 74.50€', async () => {
    let newCheckout = new Checkout(rules)
    newCheckout.scan('VOUCHER')
    newCheckout.scan('TSHIRT')
    newCheckout.scan('VOUCHER')
    newCheckout.scan('VOUCHER')
    newCheckout.scan('MUG')
    newCheckout.scan('TSHIRT')
    newCheckout.scan('TSHIRT')
    let total = newCheckout.total()
    expect(total).toBe(74.5)
  })

})
