'use strict'

process.setMaxListeners(0)

const { matchers } = require('jest-json-schema')
expect.extend(matchers)

jest.mock('./../../src/parameters.json', () => {
  let parameters = Object.assign({}, require('../extra/parameters.json'))
  return parameters
})

beforeAll(() => {
  // Put your code here
  return Promise.resolve()
})

afterAll(() => {
  // Put your code here
  return Promise.resolve()
})
