module.exports = {
    root: true,
    "env": {
        "node": true
    },    
    extends: [
        'eslint:recommended',
        'standard',
        'plugin:jest/recommended',
        'prettier',
        'prettier/standard',
        "plugin:vue/essential",
        "eslint:recommended"        
    ],
    rules: {
        // 'no-var': 1,
        // 'prefer-const': 1,
        'no-console': 1,
    },
    "parserOptions": {
        "parser": "babel-eslint"
    }
}